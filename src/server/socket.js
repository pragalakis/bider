const products = require('./api/controllers/productController.js');
const auctions = require('./api/controllers/auctionController.js');
const users = require('./api/controllers/userController.js');

module.exports = io => {
  // Get auction price given the id
  const getPrice = async id => {
    try {
      const price = await auctions.getAuctionPrice(id);
      return price;
    } catch (err) {
      console.log(err);
    }
  };

  // Get user credit given the id
  const getCredit = async id => {
    try {
      const credit = await users.getUserCredit(id);
      return credit;
    } catch (err) {
      console.log(err);
    }
  };

  // Updates auction price & user's credit
  const transaction = async (id, sum) => {
    try {
      // Subtract user's credit
      await users.subtractCredit(id, sum);

      // Increment auction price
      await auctions.incrementPrice(id, sum);
    } catch (err) {
      console.log(err);
    }
  };

  // socket connection
  io.on('connection', async socket => {
    try {
      // initialize price
      const price = await getPrice(1); // init to idi apo to auctions db
      socket.emit('price', { price: price });

      const credit = await getCredit(1);
      if (credit >= price) {
        // emit socket
        socket.on('price', function(data) {
          io.emit('price', data);

          // make the transaction
          transaction(1, data);
        });
      }

      socket.on('disconnect', function() {
        console.log('Client disconnected.');
      });
    } catch (err) {
      console.log(err);
    }
  });
};
