const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);

// api routes
require('./api/routes.js')(app);

// sockets
require('./socket.js')(io);

app.use(function(error, req, res, next) {
  console.error(error.stack);
  res.status(400).send(error.message);
});

const PORT = process.env.PORT || 8080;
server.listen(PORT, () => console.log('App is listening at port 8080'));
