module.exports = () => {
  const Sequelize = require('sequelize');

  const sequelize = new Sequelize({
    database: 'development',
    username: 'postgres',
    password: 'password',
    dialect: 'postgres',
    host: 'localhost',
    port: 5432,
    define: {
      timestamps: false,
      freezeTableName: true // disable auto capitalization of table names
    }
  });

  // Connect to the database
  sequelize
    .authenticate()
    .then(() => {
      console.log('Database connected.');
    })
    .catch(err => {
      console.log(err);
    });

  return sequelize;
};
