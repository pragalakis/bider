const User = require('../models/user.js');

// Get all users
exports.getUsers = async (req, res) => {
  try {
    const user = await User.findAll();
    res.json(user);
  } catch (err) {
    console.log(err);
  }
};

// Get single user by ID
exports.getSingleUser = async (req, res) => {
  try {
    const id = req.params.id;
    const user = await User.findByPk(id);
    res.json(user);
  } catch (err) {
    console.log(err);
  }
};

// Add a new user
exports.addUser = async (req, res) => {
  try {
    const user = await User.create(req.body);
    res.status(200).send('User added.');
  } catch (err) {
    console.log(err);
  }
};

// Update an existing user
exports.updateUser = async (req, res) => {
  try {
    const user = await User.update(req.body, {
      where: { id: req.params.id }
    });
    res.status(200).send('User updated.');
  } catch (err) {
    console.log(err);
  }
};

// Delete a user
exports.deleteUser = async (req, res) => {
  try {
    const user = await User.destroy({ where: { id: req.params.id } });
    res.status(200).send('User deleted.');
  } catch (err) {
    console.log(err);
  }
};

// Get single user credit by ID
exports.getUserCredit = async id => {
  try {
    const user = await User.findByPk(id);
    return user.credit;
  } catch (err) {
    console.log(err);
  }
};

// Subtract sum of single user's credit
exports.subtractCredit = async (id, sum) => {
  try {
    const user = User.decrement(
      { credit: sum },
      {
        where: { user_id: id }
      }
    );
    return user;
  } catch (err) {
    console.log(err);
  }
};
