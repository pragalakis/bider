const Auction = require('../models/auction.js');

// Get all auctions
exports.getAuctions = async (req, res) => {
  try {
    const auction = await Auction.findAll();
    res.json(auction);
  } catch (err) {
    console.log(err);
  }
};

// Get single auction by ID
exports.getSingleAuction = async (req, res) => {
  try {
    const id = req.params.id;
    const auction = await Auction.findByPk(id);
    res.json(auction);
  } catch (err) {
    console.log(err);
  }
};

// Add a new auction
exports.addAuction = async (req, res) => {
  try {
    const auction = await Auction.create(req.body);
    res.status(200).send('Auction added.');
  } catch (err) {
    console.log(err);
  }
};

// Update an existing auction
exports.updateAuction = async (req, res) => {
  try {
    const auction = await Auction.update(req.body, {
      where: { id: req.params.id }
    });
    res.status(200).send('Auction updated.');
  } catch (err) {
    console.log(err);
  }
};

// Delete a auction
exports.deleteAuction = async (req, res) => {
  try {
    const auction = await Auction.destroy({ where: { id: req.params.id } });
    res.status(200).send('Auction deleted.');
  } catch (err) {
    console.log(err);
  }
};

// Get single auction price by ID
exports.getAuctionPrice = async id => {
  try {
    const auction = await Auction.findByPk(id);
    return auction.price;
  } catch (err) {
    console.log(err);
  }
};

// Increment auction price by ID
exports.incrementPrice = async (id, sum) => {
  try {
    const auction = Auction.increment(
      { price: sum },
      { where: { auction_id: id } }
    );

    return auction;
  } catch (err) {
    console.log(err);
  }
};
