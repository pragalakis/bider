const Product = require('../models/product.js');

// Get all products
exports.getProducts = async (req, res) => {
  try {
    const product = await Product.findAll();
    res.json(product);
  } catch (err) {
    console.log(err);
  }
};

// Get single product by ID
exports.getSingleProduct = async (req, res) => {
  try {
    const id = req.params.id;
    const product = await Product.findByPk(id);
    res.json(product);
  } catch (err) {
    console.log(err);
  }
};

// Add a new product
exports.addProduct = async (req, res) => {
  try {
    const product = await Product.create(req.body);
    res.status(200).send('Product added.');
  } catch (err) {
    console.log(err);
  }
};

// Update an existing product
exports.updateProduct = async (req, res) => {
  try {
    const product = await Product.update(req.body, {
      where: { id: req.params.id }
    });
    res.status(200).send('Product updated.');
  } catch (err) {
    console.log(err);
  }
};

// Delete a product
exports.deleteProduct = async (req, res) => {
  try {
    const product = await Product.destroy({ where: { id: req.params.id } });
    res.status(200).send('Product deleted.');
  } catch (err) {
    console.log(err);
  }
};
