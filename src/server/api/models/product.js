const database = require('../../config/database')();

const Product = database.define('product', {
  product_id: { type: 'INTEGER', primaryKey: true },
  name: { type: 'TEXT', allowNull: false },
  artist: { type: 'TEXT', allowNull: false },
  description: { type: 'TEXT', allowNull: false },
  product_auction_id: { type: 'INTEGER', allowNull: true },
  product_user_id: { type: 'INTEGER', allowNull: false }
});

module.exports = Product;
