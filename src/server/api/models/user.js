const database = require('../../config/database')();

const User = database.define('user', {
  user_id: { type: 'INTEGER', primaryKey: true },
  username: { type: 'TEXT', allowNull: false },
  lastname: { type: 'TEXT', allowNull: false },
  password: { type: 'VARCHAR', allowNull: false },
  credit: { type: 'FLOAT', allowNull: false },
  user_auction_id: { type: 'INTEGER', allowNull: false }
});

module.exports = User;
