const database = require('../../config/database')();

const Auction = database.define('auction', {
  auction_id: { type: 'INTEGER', primaryKey: true },
  start_time: { type: 'INTEGER', allowNull: false },
  end_time: { type: 'INTEGER', allowNull: false },
  price: { type: 'FLOAT', allowNull: false },
  auction_user_id: { type: 'INTEGER', allowNull: false },
  auction_product_id: { type: 'INTEGER', allowNull: false }
});

module.exports = Auction;
