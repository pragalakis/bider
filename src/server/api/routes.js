const express = require('express');
const bodyParser = require('body-parser');

module.exports = app => {
  // serve static files
  app.use('/public', express.static(__dirname + '/dist'));

  // create application/json parser
  const jsonParser = bodyParser.json();

  // Product routes
  const products = require('./controllers/productController.js');
  app.get('/product/all', products.getProducts);
  app.get('/product/:id', products.getSingleProduct);
  app.post('/product', jsonParser, products.addProduct);
  app.put('/product/:id', jsonParser, products.updateProduct);
  app.delete('/product/:id', products.deleteProduct);

  // User routes
  const users = require('./controllers/userController.js');
  app.get('/user/all', users.getUsers);
  app.get('/user/:id', users.getSingleUser);
  app.post('/user', jsonParser, users.addUser);
  app.put('/user/:id', jsonParser, users.updateUser);
  app.delete('/user/:id', users.deleteUser);

  // Auction routes
  const auctions = require('./controllers/auctionController.js');
  app.get('/auction/all', auctions.getAuctions);
  app.get('/auction/:id', auctions.getSingleAuction);
  app.post('/auction', jsonParser, auctions.addAuction);
  app.put('/auction/:id', jsonParser, auctions.updateAuction);
  app.delete('/auction/:id', auctions.deleteAuction);
};
