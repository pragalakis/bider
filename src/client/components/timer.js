import React from 'react';

export default ({ time, status }) => {
  // calculate (and subtract) whole days
  const days = Math.floor(time / 86400);
  time -= days * 86400;

  // calculate (and subtract) whole hours
  const hours = Math.floor(time / 3600) % 24;
  time -= hours * 3600;

  // calculate (and subtract) whole minutes
  const min = Math.floor(time / 60) % 60;
  time -= min * 60;

  // what's left is seconds
  const sec = Math.floor(time % 60);
  return (
    <React.Fragment>
      {status ? (
        <h4>
          Time left: {days} Days {hours} Hours {min} Minutes {sec} Seconds
        </h4>
      ) : (
        <h4>Time left: 00:00:00</h4>
      )}
    </React.Fragment>
  );
};
