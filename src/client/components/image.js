import React from 'react';
import balloonGirl from '../assets/banksy-girl-heart-balloon.jpg';
import imgFrame from '../assets/banksy-frame.png';

export default class Images extends React.Component {
  constructor(props) {
    super(props);
    this.offsetX = 82;
    this.offsetY = 35;
    this.imgWidth = 270;
    this.imgHeight = 444;
    this.image = {};
    this.frame = {};
  }

  componentDidUpdate(prevProps) {
    // on prop change
    if (this.props.status !== prevProps.status) {
      const canvas = this.refs.canvas;
      const context = canvas.getContext('2d');

      if (this.props.status) {
        let y = 0;

        // animate the canvas
        this.animation = setInterval(() => {
          y += 2.8;
          // stop if the dispacement is bigger than 50% of img height
          if (y > this.imgHeight * 0.5) {
            clearInterval(this.animation);
          }

          // draw the background color behind the image canvas
          context.fillStyle = '#E0BEA3';
          context.fillRect(
            this.offsetX,
            this.offsetY,
            this.imgWidth,
            this.imgHeight
          );

          // draw animated image canvas
          context.drawImage(
            this.image,
            this.offsetX,
            this.offsetY + y,
            this.imgWidth,
            this.imgHeight
          );

          // draw stripes
          context.fillStyle = '#D0CFCD';
          let lineOffset = Math.floor(this.imgWidth / 10);
          let lineWidth = 4;
          for (
            let i = this.offsetX;
            i < this.imgWidth + this.offsetX;
            i += lineOffset
          ) {
            let lineHeight = 10 - y;
            context.fillRect(
              i,
              this.imgHeight + this.offsetY + y,
              lineWidth,
              lineHeight
            );
          }

          // draw image frame
          context.drawImage(this.frame, 0, 0, 435, 520);
        }, 50);
      }
    }
  }

  componentDidMount() {
    const canvas = this.refs.canvas;
    const context = canvas.getContext('2d');

    context.canvas.width = 435;
    context.canvas.height = 820;

    // load image canvas
    this.image = new Image();
    this.image.src = balloonGirl;
    this.image.onload = () => {
      context.drawImage(
        this.image,
        this.offsetX,
        this.offsetY,
        this.imgWidth,
        this.imgHeight
      );
    };

    // load image frame
    this.frame = new Image();
    this.frame.src = imgFrame;
    this.frame.onload = () => {
      context.drawImage(this.frame, 0, 0, 435, 520);
    };
  }

  componentWillUnmount() {
    clearInterval(this.animation);
  }

  render() {
    return (
      <div className="img-wrapper">
        <canvas ref="canvas" />
      </div>
    );
  }
}
