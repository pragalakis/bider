import React from 'react';

export default ({ name, artist, price }) => (
  <div>
    <h1>{name}</h1>
    <h3>Artist: {artist}</h3>
    <p className="price">{price} $</p>
  </div>
);
