import React from 'react';

export default class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 0
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleFocus = this.handleFocus.bind(this);
  }

  handleFocus(event) {
    // on focus - select all text
    event.target.select();
  }

  handleChange(event) {
    const price = Number(event.target.value);
    if (price < 0) {
      this.setState({ value: Math.abs(price) });
    } else {
      this.setState({ value: price });
    }
  }

  handleSubmit(event) {
    const value = this.state.value;
    const price = this.props.price;

    if (value > price) {
      this.props.handlePrice(value);
    }

    event.preventDefault();
  }

  render() {
    return (
      <React.Fragment>
        {this.props.status ? (
          <form onSubmit={this.handleSubmit}>
            <label>
              <input
                onChange={this.handleChange}
                onFocus={this.handleFocus}
                value={this.state.value}
                className="input"
                type="number"
              />
            </label>
            <input className="submit" type="submit" value="Place Bid" />
          </form>
        ) : (
          <p>Auction ended.</p>
        )}
      </React.Fragment>
    );
  }
}
