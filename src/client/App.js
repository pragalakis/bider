import React, { Component } from 'react';
import './app.css';
import Image from './components/image.js';
import Timer from './components/timer.js';
import Form from './components/form.js';
import Info from './components/info.js';
const io = require('socket.io-client');
const socket = io('http://localhost:8080', { reconnect: true });

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      timerStatus: true,
      status: false,
      name: null,
      artist: null,
      price: 0,
      timer: 0
    };

    this.time = 0;
    this.handlePrice = this.handlePrice.bind(this);
  }

  componentDidMount() {
    socket.on('price', data => {
      this.setState({ price: data.price });
    });

    fetch('/product/1')
      .then(res => res.json())
      .then(data => {
        this.setState({
          name: data.name,
          artist: data.artist
        });
      })
      .catch(error => console.log(error));

    fetch('/auction/1')
      .then(res => res.json())
      .then(data => {
        this.time = data.end_time;
        this.setState({
          price: data.price
        });
      })
      .catch(error => console.log(error));

    // countdown
    const timerFunction = setInterval(() => {
      // current time in seconds
      let currentTime = new Date().getTime() / 1000;
      const countdown = this.time - currentTime;
      //const countdown = 0; // for testing

      // check if the auction is ended
      if (countdown <= 0) {
        this.setState({ timerStatus: false, status: true, timer: 0 });
        clearInterval(timerFunction);
      } else {
        this.setState({
          timerStatus: true,
          status: true,
          timer: countdown
        });
      }
    }, 1000);
  }

  handlePrice(value) {
    socket.emit('price', { price: value });
    this.setState({ price: value });
  }

  render() {
    const name = this.state.name;
    const artist = this.state.artist;
    const price = this.state.price;
    const timerStatus = this.state.timerStatus;
    const status = this.state.status;
    const timer = this.state.timer;

    return (
      <div className="row">
        <div className="column">
          <Image status={!timerStatus} />
        </div>
        <div className="column info">
          <Info name={name} artist={artist} price={price} />
          {status == false ? (
            <p>loading..</p>
          ) : (
            <React.Fragment>
              <Timer status={timerStatus} time={timer} />
              <Form
                price={price}
                status={timerStatus}
                handlePrice={this.handlePrice}
              />
            </React.Fragment>
          )}
        </div>
      </div>
    );
  }
}
